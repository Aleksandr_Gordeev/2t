<!doctype html>
<html lang='en'>
<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>

	<link rel='stylesheet' href='css/bootstrap-3.3.6.min.css'>
	<link rel='stylesheet' href='css/bootstrap-select.min.css'>
	<link rel='stylesheet' href='css/2t.css'>

	<script type='text/javascript' language='javascript' src='js/jquery-2.2.0.min.js'></script>
	<script type='text/javascript' language='javascript' src='js/bootstrap-3.3.6.min.js'></script>
	<script type='text/javascript' language='javascript' src='js/bootstrap-select.min.js'></script>
	<script type='text/javascript' language='javascript' src='js/2t.js'></script>
</head>
<body>

<div class='row col-sm-12'>
	<div class='col-sm-4 panel panel-default formAdd'>
		<div class='panel-heading'>Форма добавления пользователями</div>
		<div class='panel-body'>
			<input class='form-control' id='userLogin' title='Введите логин' placeholder='Введите логин'>
		</div>
		<div class='panel-footer'>
			<button type='button' class='btn btn-default' id='btnAddUser'>Добавить пользователя</button>
		</div>
	</div>
	<div class='col-sm-3 panel panel-default formSet'>
		<div class='panel-heading'>Форма выбора пользователя</div>
		<div class='panel-body'>
			<select class='form-control selectpicker' id='curUser'>
			</select>
		</div>
		<div class='panel-footer'>
			<button type='button' class='btn btn-default' id='btnAddActivity'>Добавить активность</button>
			<button type='button' class='btn btn-default' id='btnDelUser'>Удалить пользователя</button>
		</div>
	</div>
	<div class='col-sm-4 panel panel-default formShow'>
		<div class='panel-heading'>История авторизаций</div>
		<div class='panel-body'>
			<table class="table table-hover" id='tblHistory'>
			<thead>
				<tr>
					<th>ID</th>
					<th>Дата</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
			</table>
		</div>
	</div>
</div>

</body>
</html>
