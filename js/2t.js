$(function() {
	$("#userLogin").change(function() {
		$(".formAdd").find(".btn").removeClass("btn-success");
		$(".formAdd").find(".btn").addClass("btn-danger");
		});

	$("#curUser").change(fGetUserActivity);
	$("#btnAddUser").click(fAddUser);
	$("#btnDelUser").click(fDelUser);
	$("#btnAddActivity").click(fAddActivity);

	fGetUsers();
});

function fGetUsers(){
$.ajax({
	cache: false,
	async: false,
	type: "POST",
	url: "helper.php",
	dataType: "json",
	data: {
		action: "getUsers",
		},
	success: function(data) {
		$('#curUser').empty().append("<option selected disabled>Выберите пользователя</option>");

		if ( data.error > 0 ) {
			alert(data.caption);
			return false;
			};

		$(data.users).each(function(i, e) {
			$('#curUser').append('<option value="' + e.id + '">' + e.name + '</option>');
			});

		$('#curUser').selectpicker('refresh');
		},
	error: function( jqXhr, textStatus, errorThrown ) {
		alert(errorThrown);
		return false;
		}
	});
}

function fAddUser() {
$.ajax({
	cache: false,
	async: false,
	type: "POST",
	url: "helper.php",
	dataType: "json",
	data: {
		action: "addUser",
		params: {
			"login":$("#userLogin").val(),
			},
		},
	success: function(data) {
		if ( data.error > 0 ) {
			alert(data.caption);
			return false;
			};

		$(".formAdd").find(".btn").addClass("btn-success");
		$(".formAdd").find(".btn").removeClass("btn-danger");

		fGetUsers();
		},
	error: function( jqXhr, textStatus, errorThrown ) {
		alert(errorThrown);
		return false;
		}
	});
};

function fGetUserActivity(){
$.ajax({
	cache: false,
	async: false,
	type: "POST",
	url: "helper.php",
	dataType: "json",
	data: {
		action: "getActivity",
		params: {
			"id":$("#curUser").val(),
			},
		},
	success: function(data) {
		if ( data.error > 0 ) {
			alert(data.caption);
			return false;
			};

		$("#tblHistory tbody").empty();

		$(data.activities).each(function(i, e) {
			$("#tblHistory tbody").append('<tr><td>' + e.id + '</td><td>' + e.time + '</td></tr>');
			});
		},
	error: function( jqXhr, textStatus, errorThrown ) {
		alert(errorThrown);
		return false;
		}
	});
}

function fAddActivity() {
$.ajax({
	cache: false,
	async: false,
	type: "POST",
	url: "helper.php",
	dataType: "json",
	data: {
		action: "addActivity",
		params: {
			"id":$("#curUser").val()
			},
		},
	success: function(data) {
		if ( data.error > 0 ) {
			alert(data.caption);
			return false;
			};

		fGetUserActivity();
		},
	error: function( jqXhr, textStatus, errorThrown ) {
		alert(errorThrown);
		return false;
		}
	});
};

function fDelUser(){
$.ajax({
	cache: false,
	async: false,
	type: "POST",
	url: "helper.php",
	dataType: "json",
	data: {
		action: "delUser",
		params: {
			"id":$("#curUser").val(),
			},
		},
	success: function(data) {
		if ( data.error > 0 ) {
			alert(data.caption);
			return false;
			};

		fGetUsers();
		},
	error: function( jqXhr, textStatus, errorThrown ) {
		alert(errorThrown);
		return false;
		}
	});
}
