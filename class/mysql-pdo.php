<?php
class BD {
private $conn;
private $root_dir;
private $log_file;

public function __construct($host, $user, $password) {
$this->root_dir = realpath(dirname(__FILE__)."/../")."/";
$this->log_file = $this->root_dir."log/mysql_error.log";

try {
	BD::getConnectMySQL($host, $user, $password);
} catch (Exception $e) {
	$this->addMessage($e->getMessage());
	die("Нет соединения с БД".PHP_EOL);
	};
}

private function getConnectMySQL($host, $user, $password) {
$this->conn = new PDO("mysql:host=$host;dbname=2t;charset=utf8", $user, $password);
$this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
if ( !$this->conn ) throw new Exception(mysql_error());
}

private function addMessage($error) {
$f = @fopen($this->log_file, "a");
if ( $f ) {
	fwrite($f, date("[Y-m-d H:i:s]")." [".getmypid()."] ".trim($error)."\n");
	fclose($f);
	};
}

private function execQuery($query) {
try {
	$result = $this->conn->query($query);
} catch ( PDOException $e ) {
	$this->addMessage("query>".$query);
	$this->addMessage($e);
	return array("error" => 1, "caption" => "Ошибка выполнения запроса");
	};

return $result;
}

public function getUsers() {
$result = array();

$query = "SELECT id, name
		FROM users
		ORDER BY name;";
$sqlres = $this->execQuery($query);

if ( is_array($sqlres) and $sqlres["error"] > 0 ) return $sqlres;

if ( ! $sqlres->rowCount() ) return $result;

while ( $row = $sqlres->fetchObject() ) {
	$result[] = array(
		"id" => $row->id,
		"name" => $row->name,
		);
	};

return array("error" => 0, "data" => $result);
}

public function addUser($login) {
$query = "INSERT INTO users (name) VALUES ('".@mysql_escape_string($login)."');";
$sqlres = $this->execQuery($query);

if ( is_array($sqlres) and $sqlres["error"] > 0 ) return $sqlres;

if ( ! $sqlres->rowCount() ) return array("error" => 1, "caption" => "Пользователь не добавлен");

return array("error" => 0);
}

public function delUser($id) {
$query = "DELETE FROM users WHERE id = ".(int)$id.";";
$sqlres = $this->execQuery($query);

if ( is_array($sqlres) and $sqlres["error"] > 0 ) return $sqlres;

if ( ! $sqlres->rowCount() ) return array("error" => 1, "caption" => "Пользователь не удален");

return array("error" => 0);
}

public function addActivity($id) {
$query = "INSERT INTO login_stat (user_id) VALUES (".(int)$id.");";
$sqlres = $this->execQuery($query);

if ( is_array($sqlres) and $sqlres["error"] > 0 ) return $sqlres;

if ( ! $sqlres->rowCount() ) return array("error" => 1, "caption" => "Активность не добавлена");

return array("error" => 0);
}

public function getActivity($id) {
$result = array();

$query = "SELECT ls.id, ls.time_id
		FROM users AS us
		RIGHT JOIN login_stat AS ls ON ls.user_id = us.id
		WHERE us.id = ".(int)$id."
		ORDER BY time_id DESC;";
$sqlres = $this->execQuery($query);

if ( is_array($sqlres) and $sqlres["error"] > 0 ) return $sqlres;

if ( ! $sqlres->rowCount() ) return $result;

while ( $row = $sqlres->fetchObject() ) {
	$result[] = array(
		"id" => $row->id,
		"time" => $row->time_id,
		);
	};

return array("error" => 0, "data" => $result);
}

}
