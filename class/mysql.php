<?php
class BD {
private $conn;
private $root_dir;
private $log_file;

public function __construct($host, $user, $password) {
$this->root_dir = realpath(dirname(__FILE__)."/../")."/";
$this->log_file = $this->root_dir."log/mysql_error.log";

try {
	BD::getConnectMySQL($host, $user, $password);
} catch (Exception $e) {
	$this->addMessage($e->getMessage());
	die("Нет соединения с БД");
	}
}

private function getConnectMySQL($host, $user, $password) {
$this->conn = @mysql_connect($host, $user, $password);
if ( !$this->conn ) throw new Exception(mysql_error());
}

private function addMessage($error) {
$f = @fopen($this->log_file, "a");
if ( $f ) {
	fwrite($f, date("[Y-m-d H:i:s]")." [".getmypid()."] ".trim($error)."\n");
	fclose($f);
	};
}

private function execQuery($query) {
$result = @mysql_query($query, $this->conn);
if ( ! $result ) {
	$this->addMessage(mysql_error());
	return array("error" => 1, "caption" => "Ошибка выполнения запроса");
	};

return $result;
}

public function getUsers() {
$result = array();

$query = "SELECT id, name
		FROM 2t.users
		ORDER BY name;";
$sqlres = $this->execQuery($query);

if ( $sqlres["error"] > 0 ) return $sqlres;

if ( ! @mysql_num_rows($sqlres) ) return $result;

while ( $row = @mysql_fetch_object($sqlres) ) {
	$result[] = array(
		"id" => $row->id,
		"name" => $row->name,
		);
	};

return array("error" => 0, "data" => $result);
}

public function addUser($login) {
$query = "INSERT INTO 2t.users (name) VALUES ('".@mysql_escape_string($login)."');";
$sqlres = $this->execQuery($query);

if ( $sqlres["error"] > 0 ) return $sqlres;

if ( !mysql_affected_rows($this->conn) ) return array("error" => 1, "caption" => "Пользователь не добавлен");

return array("error" => 0);
}

public function delUser($id) {
$query = "DELETE FROM 2t.users WHERE id = ".(int)$id.";";
$sqlres = $this->execQuery($query);

if ( $sqlres["error"] > 0 ) return $sqlres;

if ( !mysql_affected_rows($this->conn) ) return array("error" => 1, "caption" => "Пользователь не удален");

return array("error" => 0);
}

public function addActivity($id) {
$query = "INSERT INTO 2t.login_stat (user_id) VALUES (".(int)$id.");";
$sqlres = $this->execQuery($query);

if ( $sqlres["error"] > 0 ) return $sqlres;

if ( !mysql_affected_rows($this->conn) ) return array("error" => 1, "caption" => "Активность не добавлена");

return array("error" => 0);
}

public function getActivity($id) {
$result = array();

$query = "SELECT ls.id, ls.time_id
		FROM 2t.users AS us
		RIGHT JOIN 2t.login_stat AS ls ON ls.user_id = us.id
		WHERE us.id = ".(int)$id."
		ORDER BY time_id DESC;";
$sqlres = $this->execQuery($query);

if ( $sqlres["error"] > 0 ) return $sqlres;

if ( ! @mysql_num_rows($sqlres) ) return $result;

while ( $row = mysql_fetch_object($sqlres) ) {
	$result[] = array(
		"id" => $row->id,
		"time" => $row->time_id,
		);
	};

return array("error" => 0, "data" => $result);
}

}
