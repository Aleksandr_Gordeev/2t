<?php
class Users {
private $bd;
private $memcached;

public function __construct($bd, $memcached) {
$this->bd = $bd;
$this->memcached = $memcached;
}

public function getUsers() {
$result = $this->memcached->getUsers();

if ( empty($result) ) {
	$users = $this->bd->getUsers();

	if ( $users["error"] > 0 ) return $users;

	$result = $users["data"];

	$this->memcached->setUsers($result);
	};

return array("error" => 0, "users" => $result);
}

public function addUser($params) {
if ( !isset($params["login"]) ) return array("error" => 1, "caption" => "Не указано имя пользователя");
return $this->bd->addUser($params["login"]);
}

public function delUser($params) {
if ( !isset($params["id"]) ) return array("error" => 1, "caption" => "Не указан идентификатор пользователя");
return $this->bd->delUser($params["id"]);
}

public function addActivity($params) {
if ( !isset($params["id"]) ) return array("error" => 1, "caption" => "Не указан идентификатор пользователя");
return $this->bd->addActivity($params["id"]);
}

public function getActivity($params) {
if ( !isset($params["id"]) ) return array("error" => 1, "caption" => "Не указан идентификатор пользователя");

$result = $this->memcached->getActivity($params["id"]);

if ( empty($result) ) {
	$activity = $this->bd->getActivity($params["id"]);

	if ( $activity["error"] > 0 ) return $activity;

	$result = $activity["data"];

	$this->memcached->setActivity($params["id"], $result);
	};

return array("error" => 0, "activities" => $result);
}

}
