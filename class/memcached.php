<?php
class myMemcached {
private $cache;
private $ttl;

public function __construct($host, $port, $ttl) {
$this->cache = new Memcached;
$this->cache->addServer($host, $port);
if ( $this->cache->getStats()[$host.":".$port]["pid"] == -1 ) die("Нет соединения с Memcached".PHP_EOL);
}

public function getUsers() {
return $this->cache->get("Users");
}

public function setUsers($data) {
$this->cache->set("Users", $data, $this->ttl);
}

public function getActivity($id) {
return $this->cache->get("UserActivity".(int)$id);
}

public function setActivity($id, $data) {
$this->cache->set("UserActivity".(int)$id, $data, $this->ttl);
}

}
