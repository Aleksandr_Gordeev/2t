<?php
if ( !isset($_REQUEST["action"]) or empty($_REQUEST["action"]) ) die("Не указано действие".PHP_EOL);

if ( !isset($_REQUEST["params"]) or empty($_REQUEST["params"]) ) $_REQUEST["params"] = array();

#require_once "class/mysql.php";
require_once "class/mysql-pdo.php";
require_once "class/memcached.php";
require_once "class/users.php";

$bd = new BD("localhost", "root", null);
$memcached = new myMemcached("127.0.0.1", 11211, 30);
$users = new Users($bd, $memcached);

if ( !method_exists($users, $_REQUEST["action"]) ) die("Неизвестное действие".PHP_EOL);

echo json_encode($users->$_REQUEST["action"]($_REQUEST["params"]), JSON_UNESCAPED_UNICODE);
?>
